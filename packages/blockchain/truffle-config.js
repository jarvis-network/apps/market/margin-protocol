require('dotenv').config();

const HDWalletProvider = require('truffle-hdwallet-provider');
const infuraApikey = process.env.INFURA_PROJECT_ID;
const mnemonic = process.env.MNEMONIC;

function createNetworkDef(networkId) {
  const networkNames = {
    1: 'mainnet',
    3: 'ropsten',
    42: 'kovan',
  };
  const networkName = networkNames[networkId];
  return {
    provider: () =>
      new HDWalletProvider(
        mnemonic,
        `https://${networkName}.infura.io/v3/${infuraApikey}`,
        0,
        10,
      ),
    network_id: networkId,
    gas: 300000,
    gasPrice: 10000000000,
  };
}

module.exports = {
  mocha: {
    enableTimeouts: false,
  },
  plugins: ['truffle-security'],
  networks: {
    development: {
      host: '127.0.0.1',
      port: 8545,
      network_id: 5777,
      gas: 300000,
      gasPrice: 10000000000,
    },
    ropsten: createNetworkDef(3),
    kovan: createNetworkDef(42),
  },
};
