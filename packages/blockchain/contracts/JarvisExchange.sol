pragma solidity ^0.6.0;

import '@openzeppelin/contracts-ethereum-package/contracts/math/SafeMath.sol';
import '@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/IERC20.sol';
import './Pausable.sol';

pragma experimental ABIEncoderV2;


contract JarvisExchange is Pausable {
  using SafeMath for uint256;

  //address of trading platform
  address public tradingPlatform;

  //constant name of the solution
  string public constant name = 'JarvisExchange';

  //constant version of the solution
  string public constant version = '1';

  //domain separator according to EIP712
  bytes32 public DOMAIN_SEPARATOR;

  // bytes32 public constant USERDEPOSIT_TYPEHASH = keccak256("userDeposit(address token,address userOrLp,uint256 amount,uint256 nonce,uint256 expiry)");
  bytes32 public constant USERDEPOSIT_TYPEHASH = 0x0e19c6061dc841b4c0facc3c0c5f15813e45e671395e24072dbcf686184f5b4b;

  // bytes32 public constant LPDEPOSIT_TYPEHASH = keccak256("liquidityProviderDeposit(address token,address userOrLp,uint256 amount,uint256 nonce,uint256 expiry)");
  bytes32 public constant LPDEPOSIT_TYPEHASH = 0x29346ae468f7e20e5e2dd92a54e150ea861d08deeef6228181ca5977d6991433;

  //bytes32 public constant USERWITHDRAW_TYPEHASH = keccak256("userWithdraw(address token,address user,address LP,uint256 amount,uint256 gain,uint256 loss,uint256 daoFee,uint256 LPFee,uint256 nonce,uint256 expiry)");
  bytes32 public constant USERWITHDRAW_TYPEHASH = 0xbe59ef6a74ee3b172b4cbb345bf26c295ae53be82ca11a113d55061c5778e082;

  //bytes32 public constant LPWITHDRAW_TYPEHASH = keccak256("liquidityProviderWithdraw(address token,address user,address LP,uint256 amount,uint256 gain,uint256 loss,uint256 daoFee,uint256 LPFee,uint256 nonce,uint256 expiry)");
  bytes32 public constant LPWITHDRAW_TYPEHASH = 0xef0edb7722f3bd4be81a4ae48b00f90dd21d6d91a9301e3d2ffc0383c11c3e76;

  //nonce for trading-platform metasignature of every address
  mapping(address => uint256) public nonces;

  //map of supported tokens
  mapping(address => bool) public tokensSupported;

  //map of LP supported
  mapping(address => bool) public LPSupported;

  //map (token, userAddress) that returns the token balance of that user address
  mapping(address => mapping(address => uint256)) public userBalance;

  //map (token, LPAddress) that returns the token balance of that LP address
  mapping(address => mapping(address => uint256)) public LPBalance;

  //fees destinated to the Dao
  mapping(address => uint256) public daoFee;

  //fees destinated to the LPs
  mapping(address => uint256) public totalLPFees;

  modifier IsLiquidityProvider {
    require(LPSupported[msg.sender], 'Sender must be a liquidity Provider');
    _;
  }

  modifier IsNotLiquidityProvider {
    require(
      !LPSupported[msg.sender],
      'Sender must not be a liquidity Provider'
    );
    _;
  }

  struct Deposit {
    address token;
    address userOrLP;
    uint256 amount;
    uint256 nonce;
    uint256 expiry;
    uint8 v;
    bytes32 r;
    bytes32 s;
  }

  struct Withdraw {
    address token;
    address user;
    address LP;
    uint256 amount;
    uint256 gain;
    uint256 loss;
    uint256 daoFee;
    uint256 LPFee;
    uint256 nonce;
    uint256 expiry;
    uint8 v;
    bytes32 r;
    bytes32 s;
  }

  event UserDeposit(
    address indexed _token,
    address indexed _user,
    uint256 _actualDeposit,
    uint256 _totalDeposit
  );

  event LiquidityProviderDeposit(
    address indexed _token,
    address indexed _LP,
    uint256 _actualDeposit,
    uint256 _totalDeposit
  );

  event UserWithdraw(
    address indexed _token,
    address indexed _user,
    address indexed _counterPart,
    uint256 _amountWithdrawn,
    uint256 _UserGain,
    uint256 _UserLoss,
    uint256 _daoFee,
    uint256 _LPFee,
    uint256 _actualDeposit
  );

  event LiquidityProviderWithdraw(
    address indexed _token,
    address indexed _LP,
    address indexed _counterPart,
    uint256 _amountWithdrawn,
    uint256 _LPGain,
    uint256 _LPLoss,
    uint256 _daoFee,
    uint256 _LPFee,
    uint256 _actualDeposit
  );

  event LiquidityProviderFeeWithdraw(
    address indexed _token,
    address indexed _LP,
    uint256 _amountWithdrawn,
    uint256 _actualFees
  );

  /**
   * @dev Initializes the proxy contract setting Dai address, Lp address, Trading Platform address and Id of the chain
   */
  function initialize(
    address _daiAddress,
    address _LPaddress,
    address _tradingPlatform,
    uint256 _chainId
  ) external {
    Pausable.initialize(_tradingPlatform);
    require(
      _daiAddress != address(0) &&
        _LPaddress != address(0) &&
        _tradingPlatform != address(0),
      'Input addresses can not be null'
    );
    tokensSupported[_daiAddress] = true;
    LPSupported[_LPaddress] = true;
    tradingPlatform = _tradingPlatform;
    DOMAIN_SEPARATOR = keccak256(
      abi.encode(
        keccak256(
          'EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)'
        ),
        keccak256(bytes(name)),
        keccak256(bytes(version)),
        _chainId,
        address(this)
      )
    );
  }

  /**
   * @dev Allow users to deposit Dai on Trading Platform
   */
  function userDeposit(Deposit calldata _depositValues)
    external
    whenNotPaused
    IsNotLiquidityProvider
  {
    _deposit(USERDEPOSIT_TYPEHASH, _depositValues);
    userBalance[_depositValues.token][_depositValues.userOrLP] += _depositValues
      .amount;
    emit UserDeposit(
      _depositValues.token,
      _depositValues.userOrLP,
      _depositValues.amount,
      userBalance[_depositValues.token][_depositValues.userOrLP]
    );
  }

  /**
   * @dev Allow LP to deposit Dai on Trading Platform
   */
  function liquidityProviderDeposit(Deposit calldata _depositValues)
    external
    whenNotPaused
    IsLiquidityProvider
  {
    _deposit(LPDEPOSIT_TYPEHASH, _depositValues);
    LPBalance[_depositValues.token][_depositValues.userOrLP] += _depositValues
      .amount;
    emit LiquidityProviderDeposit(
      _depositValues.token,
      _depositValues.userOrLP,
      _depositValues.amount,
      LPBalance[_depositValues.token][_depositValues.userOrLP]
    );
  }

  function _deposit(bytes32 _typeHash, Deposit memory _depositValues) internal {
    bytes32 digest = keccak256(
      abi.encodePacked(
        '\x19\x01',
        DOMAIN_SEPARATOR,
        keccak256(
          abi.encode(
            _typeHash,
            _depositValues.token,
            _depositValues.userOrLP,
            _depositValues.amount,
            _depositValues.nonce,
            _depositValues.expiry
          )
        )
      )
    );
    require(tokensSupported[_depositValues.token], 'Input token not supported');
    require(
      _depositValues.userOrLP == msg.sender,
      'Liquidity provider or user must be the senders of the transaction'
    );
    require(_depositValues.amount > 0, 'Input amount must bigger than zero');
    require(
      _depositValues.expiry == 0 || now <= _depositValues.expiry,
      'Meta-signature expired'
    );
    require(
      _depositValues.nonce == nonces[_depositValues.userOrLP]++,
      'Invalid nonce'
    );
    require(
      tradingPlatform ==
        ecrecover(digest, _depositValues.v, _depositValues.r, _depositValues.s),
      'Invalid meta-signature of the trading platform'
    );
    IERC20 token = IERC20(_depositValues.token);
    require(
      token.transferFrom(
        _depositValues.userOrLP,
        address(this),
        _depositValues.amount
      ),
      'Wrong transferFrom call'
    );
  }

  /**
   * @dev Allow users to withdraw Dai from Trading Platform
   */
  function userWithdraw(Withdraw calldata _withdrawValues)
    external
    whenNotPaused
    IsNotLiquidityProvider
  {
    _withdraw(USERWITHDRAW_TYPEHASH, _withdrawValues);
    emit UserWithdraw(
      _withdrawValues.token,
      _withdrawValues.user,
      _withdrawValues.LP,
      _withdrawValues.amount,
      _withdrawValues.gain,
      _withdrawValues.loss,
      _withdrawValues.daoFee,
      _withdrawValues.LPFee,
      userBalance[_withdrawValues.token][_withdrawValues.user]
    );
  }

  /**
   * @dev Allow LP to withdraw Dai from Trading Platform
   */
  function liquidityProviderWithdraw(Withdraw[] calldata _withdrawValues)
    external
    whenNotPaused
    IsLiquidityProvider
  {
    for (uint256 j = 0; j < _withdrawValues.length; j++) {
      _withdraw(LPWITHDRAW_TYPEHASH, _withdrawValues[j]);
      emit LiquidityProviderWithdraw(
        _withdrawValues[j].token,
        _withdrawValues[j].LP,
        _withdrawValues[j].user,
        _withdrawValues[j].amount,
        _withdrawValues[j].loss,
        _withdrawValues[j].gain,
        _withdrawValues[j].daoFee,
        _withdrawValues[j].LPFee,
        LPBalance[_withdrawValues[j].token][_withdrawValues[j].LP]
      );
    }
  }

  function _withdraw(bytes32 _typeHash, Withdraw memory _withdrawValues)
    internal
  {
    bytes32 digest = keccak256(
      abi.encodePacked(
        '\x19\x01',
        DOMAIN_SEPARATOR,
        keccak256(
          abi.encode(
            _typeHash,
            _withdrawValues.token,
            _withdrawValues.user,
            _withdrawValues.LP,
            _withdrawValues.amount,
            _withdrawValues.gain,
            _withdrawValues.loss,
            _withdrawValues.daoFee,
            _withdrawValues.LPFee,
            _withdrawValues.nonce,
            _withdrawValues.expiry
          )
        )
      )
    );
    require(
      _withdrawValues.expiry == 0 || now <= _withdrawValues.expiry,
      'Meta-signature expired'
    );
    require(
      tradingPlatform ==
        ecrecover(
          digest,
          _withdrawValues.v,
          _withdrawValues.r,
          _withdrawValues.s
        ),
      'Invalid meta-signature of the trading platform'
    );
    if (_typeHash == USERWITHDRAW_TYPEHASH) {
      _userWithDraw(_withdrawValues);
    } else {
      _liquidityProviderWithDraw(_withdrawValues);
    }
  }

  function _userWithDraw(Withdraw memory _withdrawValues) internal {
    require(
      _withdrawValues.user == msg.sender,
      'User must be the sender of the transaction'
    );
    require(
      LPSupported[_withdrawValues.LP],
      'Counterpart liquidity provider is not admitted'
    );
    require(
      _withdrawValues.nonce == nonces[_withdrawValues.user]++,
      'Invalid nonce'
    );
    IERC20 token = IERC20(_withdrawValues.token);
    uint256 totalFees = (_withdrawValues.daoFee).add(_withdrawValues.LPFee);
    uint256 newBalance = userBalance[_withdrawValues.token][_withdrawValues
      .user]
      .add(_withdrawValues.gain)
      .sub((_withdrawValues.loss).add(totalFees));
    userBalance[_withdrawValues.token][_withdrawValues.user] = newBalance.sub(
      _withdrawValues.amount
    );
    if (_withdrawValues.amount > 0) {
      require(
        token.transfer(_withdrawValues.user, _withdrawValues.amount),
        'Wrong transfer call'
      );
    }
    uint256 LPActualBalance = LPBalance[_withdrawValues.token][_withdrawValues
      .LP];
    LPBalance[_withdrawValues.token][_withdrawValues.LP] = (
      (LPActualBalance.add(_withdrawValues.loss)).sub(_withdrawValues.gain)
    );
    daoFee[_withdrawValues.token] = daoFee[_withdrawValues.token].add(
      _withdrawValues.daoFee
    );
    totalLPFees[_withdrawValues.token] = totalLPFees[_withdrawValues.token].add(
      _withdrawValues.LPFee
    );
  }

  function _liquidityProviderWithDraw(Withdraw memory _withdrawValues)
    internal
  {
    require(
      _withdrawValues.LP == msg.sender,
      'Liquidity provider must be the sender of the transaction'
    );
    require(
      !LPSupported[_withdrawValues.user],
      'Counterpart user can not be a liquidity provider'
    );
    require(
      _withdrawValues.nonce == nonces[_withdrawValues.LP]++,
      'Invalid nonce'
    );
    IERC20 token = IERC20(_withdrawValues.token);
    uint256 totalFees = (_withdrawValues.daoFee).add(_withdrawValues.LPFee);
    uint256 newBalance = userBalance[_withdrawValues.token][_withdrawValues
      .user]
      .add(_withdrawValues.gain)
      .sub((_withdrawValues.loss).add(totalFees));
    userBalance[_withdrawValues.token][_withdrawValues.user] = newBalance;
    uint256 LPActualBalance = LPBalance[_withdrawValues.token][_withdrawValues
      .LP];
    LPBalance[_withdrawValues.token][_withdrawValues.LP] = LPActualBalance
      .add(_withdrawValues.loss)
      .sub(_withdrawValues.gain)
      .sub(_withdrawValues.amount);
    if (_withdrawValues.amount > 0) {
      require(
        token.transfer(_withdrawValues.LP, _withdrawValues.amount),
        'Wrong transfer call'
      );
    }
    daoFee[_withdrawValues.token] = daoFee[_withdrawValues.token].add(
      _withdrawValues.daoFee
    );
    totalLPFees[_withdrawValues.token] = totalLPFees[_withdrawValues.token].add(
      _withdrawValues.LPFee
    );
  }

  function withdrawLiquidityProviderFee(address _token, uint256 _amount)
    external
    whenNotPaused
    IsLiquidityProvider
  {
    address LP = msg.sender;
    uint256 newFeeBalance = totalLPFees[_token].sub(_amount);
    totalLPFees[_token] = newFeeBalance;
    IERC20 token = IERC20(_token);
    require(token.transfer(LP, _amount), 'Wrong transfer call');
    emit LiquidityProviderFeeWithdraw(_token, LP, _amount, newFeeBalance);
  }
}
