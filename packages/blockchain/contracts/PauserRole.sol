pragma solidity ^0.6.0;

import '@openzeppelin/upgrades/contracts/Initializable.sol';
import './Roles.sol';


/**
This contract is based on the Openzeppelin PauserRole one, but we have removed addPauser and removePauser
functions in order to increase the security and we have updated the pragma version from 5.0.0 to 6.0.0
*/

contract PauserRole is Initializable {
  using Roles for Roles.Role;

  event PauserAdded(address indexed account);
  event PauserRemoved(address indexed account);

  Roles.Role private _pausers;

  function initialize(address sender) internal virtual initializer {
    if (!isPauser(sender)) {
      _addPauser(sender);
    }
  }

  modifier onlyPauser() {
    require(isPauser(msg.sender));
    _;
  }

  function isPauser(address account) public view returns (bool) {
    return _pausers.has(account);
  }

  function _addPauser(address account) internal {
    _pausers.add(account);
    emit PauserAdded(account);
  }

  uint256[50] private ______gap;
}
