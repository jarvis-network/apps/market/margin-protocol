pragma solidity ^0.6.0;

import '@openzeppelin/upgrades/contracts/Initializable.sol';
import './PauserRole.sol';


/**
This contract is based on the Openzeppelin Pausable one, but the unpause function is removed in order to make
the pause immutable when is activated,to prevent an attacker that steals private key to unpause the contract
and continue the attack
*/

contract Pausable is Initializable, PauserRole {
  event Paused(address account);
  event Unpaused(address account);

  bool private _paused;

  function initialize(address sender)
    internal
    override(PauserRole)
    initializer
  {
    PauserRole.initialize(sender);

    _paused = false;
  }

  /**
   * @return true if the contract is paused, false otherwise.
   */
  function paused() public view returns (bool) {
    return _paused;
  }

  /**
   * @dev Modifier to make a function callable only when the contract is not paused.
   */
  modifier whenNotPaused() {
    require(!_paused);
    _;
  }

  /**
   * @dev Modifier to make a function callable only when the contract is paused.
   */
  modifier whenPaused() {
    require(_paused);
    _;
  }

  /**
   * @dev called by the owner to pause, triggers stopped state
   */
  function pause() public onlyPauser whenNotPaused {
    _paused = true;
    emit Paused(msg.sender);
  }

  uint256[50] private ______gap;
}
